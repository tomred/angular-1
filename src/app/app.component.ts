import { Component } from '@angular/core';

import { Post } from './interfaces/post';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent {

    posts: Post[] = [
        {
            title: 'Mon premier post',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim enim, pulvinar scelerisque ex ac, volutpat fringilla odio. Vivamus dignissim suscipit sapien et elementum. Etiam pellentesque ac turpis eu faucibus.',
            loveIts: -3,
            created_at: new Date('01/01/2017 00:00:00')
        },
        {
            title: 'Mon second post',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim enim, pulvinar scelerisque ex ac, volutpat fringilla odio. Vivamus dignissim suscipit sapien et elementum. Etiam pellentesque ac turpis eu faucibus.',
            loveIts: 5,
            created_at: new Date('01/01/2018 00:00:00')
        },
        {
            title: 'Mon troisième post',
            content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque enim enim, pulvinar scelerisque ex ac, volutpat fringilla odio. Vivamus dignissim suscipit sapien et elementum. Etiam pellentesque ac turpis eu faucibus.',
            loveIts: 0,
            created_at: new Date('01/01/2019 00:00:00')
        },
    ];

}
